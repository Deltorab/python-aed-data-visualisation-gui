#!/usr/bin/env python
# coding: utf-8

# ### Installing and importing the useful libraries

# In[1]:


import sys
runOK = True
"""
We are stacking try/except to be sure nothing is going wrong will installing and import libraries.
runOK is a boolean that is True if everything is OK and False otherwise.
"""
try:
    get_ipython().system('{sys.executable} -m pip install plotly')
    import plotly.express as px
    import plotly.graph_objects as go
    import plotly.subplots as ps
except:
    print("Fail to install plotly library")
    runOK = False

try:
    get_ipython().system('{sys.executable} -m pip install pandas')
    import pandas as pd
except:
    print("Fail to install pandas library")
    runOK = False

try:
    get_ipython().system('{sys.executable} -m pip install numpy')
    import numpy as np
except:
    print("Fail to install numpy library")
    runOK = False

try:
    get_ipython().system('{sys.executable} -m pip install matplotlib')
    import matplotlib.pyplot as plt
except:
    print("Fail to install matplotlib library")
    runOK = False 

try:
    get_ipython().system('{sys.executable} -m pip install seaborn')
    import seaborn as sns
except:
    print("Fail to install seaborn library")
    runOK = False 

try:
    get_ipython().system('{sys.executable} -m pip install PySimpleGUI')
    import PySimpleGUI as sg
except:
    print("Fail to install numpy library")
    runOK = False

runOK


# ### Super class
# In the following block of code we are creating a *super class* that is bundling all the *DataFrames* needed to convey the analysis, and also methods to display results.
# Please note that the sample is created directly into the *Class* when instantiated.

# In[2]:


class AnalyseData():
    
    def __init__(self):
        """
        In the constructor we are generating the random sample of 400.
        It is also creating the DataFrame with only breach cases,a contingency
        table between HRG and Breachornot, but also two list of conditions and choices
        to create a histogram gathered by time of stay later on.
        """
        self.data = pd.read_csv("AED4weeks.csv")
        self.df = self.data.sample(n = 400, random_state = 666)       # Draw a random sample of 400 patients
        self.df["LoSh"] = self.df["LoS"] / 60                         # Creating a new column "LoSh" containing LoS in hour
        self.df_breach = self.df[self.df["Breachornot"]=="breach"]    # Returning the DataFrame filtered by breach case
        self.df_contingency = self.df[["Breachornot", "HRG"]]         # Keeping only Breachornot and HRG from the sample
        self.contingency_tbl = pd.crosstab(self.df_contingency["Breachornot"],   # Create contingency table of Breachornot and HRG
                                           self.df_contingency["HRG"],
                                           margins = True)
        col = "LoS"
        #Stating the conditions for our np.select function
        self.conditions  = [ self.df[col] < 60, (self.df[col] >=60) & (self.df[col]< 120), (self.df[col] >=120) & (self.df[col]<= 180), self.df[col] >180 ]
        #Stating the values to go simultaneously
        self.choices     = [ "Less than one hour", 'One hour to two hour', 'two hours to three hours','more than three hour' ]
        self.df["hour_distribution"] = np.select(self.conditions, self.choices, default=np.nan)
        
        #Using lambda and nested if else to create a new column for age distribution
        self.df['Age_categorisation_1'] = self.df['Age'].apply(lambda x : "0 - 5 year old" if x >= 0 and x <=5 else ("'6 - 18 year old'" if x >= 6 and x<=18 else ("18+" if x>=18 and x<=60 else "60+")))
        
        
    def stat_and_breach(self):
        """
        stat_and_breach() is a method that plot the histogram of breachornot and
        the basic statistic description of each column.

        parameters: This methods doesn't require any arguments.
    
        return: This methods doesn't return anything.
        """
        px.pie(self.df, names='Breachornot').show()                   # Plot hist of breaches
        print(self.df.describe())                                     # Overview of stat of the sample
        
    def frequency_plots_of_breach(self):
        """
        frequency_plots_of_breach() is a method that plot histograms of Age,
        Period, HRG and nooftreatment using the plotly library.

        parameters: This methods doesn't require any arguments.
    
        return: This methods doesn't return anything.
        """
        fig = ps.make_subplots(rows=2, cols=2)                         # Creating an aera of displaying
        fig.add_trace(go.Histogram(x=self.df_breach['Age'],            # Adding Age histogram
                                   nbinsx=5, 
                                   name='Age'), row=1, col=1)
        fig.update_layout(title = "Histograms of Age, Period, HRG and nooftreatment")
        fig.add_trace(go.Histogram(x=self.df_breach['Period'],         # Adding Period histogram
                                   nbinsx=10, 
                                   name='Period'),row=1,col=2)
        fig.add_trace(go.Histogram(x=self.df_breach['HRG'],            # Adding HRG histogram
                                   name='HRG'),row=2,col=1)
        fig.add_trace(go.Histogram(x=self.df_breach['nooftreatment'], 
                                   name='nooftreatment'),row=2,col=2)
        fig.show()                                                     # Displaying all the plots
        
    def average_LOS_monthly(self):
        """
        average_LOS_monthly() is a method that is displaying a line of the time
        series representing average LoS everyday of the month.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """ 
        final = self.df.sort_values('Day')
        lt = final[["Day", "LoS"]].groupby(['Day'], as_index=False).mean()

        fig = px.line(lt, x="Day",
                      y="LoS", 
                      title='Daily Average Time Spent in Hospital')
        
        fig.add_vline(x=7)
        fig.add_vline(x=14)
        fig.add_vline(x=21)

        fig.add_annotation(dict(font=dict(color='red',size=15),
                                x=0.07,
                                y=-0.12,
                                showarrow=False, 
                                text="Week 1", 
                                textangle=0,
                                xanchor='left',
                                xref="paper",
                                yref="paper"))
        
        fig.add_annotation(dict(font=dict(color='red',size=15),
                                x=0.3,
                                y=-0.12,
                                showarrow=False,
                                text="Week 2",
                                textangle=0,
                                xanchor='left',
                                xref="paper",
                                yref="paper"))

        fig.add_annotation(dict(font=dict(color='red',size=15),
                                x=0.57,
                                y=-0.12,
                                showarrow=False,
                                text="Week 3",
                                textangle=0,
                                xanchor='left',
                                xref="paper",
                                yref="paper"))

        fig.add_annotation(dict(font=dict(color='red',size=15),
                                x=0.85,
                                y=-0.12,
                                showarrow=False,
                                text="Week 4",
                                textangle=0,
                                xanchor='left',
                                xref="paper",
                                yref="paper"))
        fig.show()
        
    def LOS_by_DayOfWeek(self):
        """
        LOS_by_DayOfWeek() is a method that is displaying a boxplot of Lenght of
        Stay for each day of the week.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """ 
        fig = px.box(data_frame=self.df,                   # LoSh vs. Day of the week 
                     x='DayofWeek', 
                     y='LoSh', 
                     title='LoS (hourly) distribution by DayOfWeek')
        fig.show()
    
    def average_patients_per_day(self):
        """
        average_patients_per_day() is a method that is displaying a timeseries
        representing the average number of patients by Period on a typical day.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """ 
        # Mean number of patients per day
        fig = px.line(self.df[["Period","noofpatients"]].groupby("Period").mean(),
                      y="noofpatients",
                      title="Average number of patients on a day")
        fig.show()
    
    def LOS_by_hour_by_age(self):
        """
        LOS_by_hour_by_age() is a method that is displaying a scatter plot
        of Lenght of Stay (in hour) by Age.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """     
        # LoSh vs. Age
        fig = px.scatter(data_frame=self.df,
                         x='Age',
                         y='LoSh',
                         title='LoS (hourly) distribution by Age')
        fig.show()
    
    def hist_LoSh(self):
        """
        hist_LoSh() is a method that is displaying a histogram
        of Lenght of Stay in hour.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """
       # LoSh frequency
        fig = px.histogram(data_frame=self.df,
                           x='LoSh', 
                           nbins=16, 
                           title='LoS (hourly) distribution')
        fig.update_layout(bargap=0.1)
        fig.show()
        
    def hist_time_spent_by_patient(self):
        """
        hist_time_spent_by_patient() is a method that is displaying a histogram
        of the time spent by patient, using custom categories of group (less than one
        hour, between 1 and 2, etc.)

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """
        plt.figure(figsize=(12,8))
        #Increasing the figure size using plt.figure
        ax = sns.countplot(x=self.df['hour_distribution'],
                           orient='h',
                           palette="Set3",
                           edgecolor=sns.color_palette("dark", 3))
        #Using seaborn library to count the values for hour_distribution according to the number of age
        ax.set(ylabel='Number of Patients')                           #Setting the ylabel
        plt.title("Hours Spent By Patients During AED")               #Setting the title
        ax.set_xlabel('')
        plt.show()                                                    #Showing the graph
        
        
    def hist_and_fitting_curve(self):
        """
        hist_and_fitting_curve() is a method that is displaying histogram
        of every numerical type of column in the Sample and try to fit it
        a distribution function.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """   
        data_hist = self.df[['Age',"Day",'Period','LoS','noofpatients']]  # Intitalising the numerical columns and saving in data
        copy_LoS = data_hist['LoS'].astype(float)
        data_hist['LoS'] = copy_LoS                                       # Changing the data type using astype function
        plt.figure(figsize=(14,14))

        for i, column in enumerate(data_hist.columns, 1):
            plt.subplot(3,3,i)                                            # Enumerating on the columns and plotting them using subplots
            sns.histplot(data_hist[column],
                         kde=True,                                        # Kde is true to plot the distribution line
                         color='red')               
            plt.xlabel(column, fontsize=15)                               # Naming x label
            plt.ylabel("Frequency",fontsize=15)                           # Naming y label
    
    def hist_by_age(self):
        """
        hist_by_age() is a method that uses a column spliting the patients
        into categories of age to display a histogram of the repartition of
        ages in the Sample.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """   
        # Using groupby 
        categorical_dataset = self.df[["Age_categorisation_1", "noofinvestigation"]].groupby(['Age_categorisation_1'], as_index=False).mean()
    
        plt.figure(figsize=(12,8))                                           # Setting size of the plot

        ax = sns.barplot(x = categorical_dataset['Age_categorisation_1'],    # Plotting the histogram with seaborn
                         y=categorical_dataset['noofinvestigation'],
                         facecolor=(0, 0, 0, 0),
                         linewidth=5,
                         edgecolor=sns.color_palette("dark", 3))

        ax.set(ylabel='Number of Patients')                                  # Setting y label
        ax.set_xlabel('')                                                    # Setting x label
    
    def analyse_breach_HRG(self):
        """
        analyse_breach_HRG() is a method that plot a contingency table of HRG and breachornot.
        We then calculcate the ratio of number of breach by HRG compare to total breach.
        
        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """       
        print(self.contingency_tbl)
        # To calculate the breach ratio of each HRG; this will divide the element by another list's element with same index.
        self.HRG_ratio = np.array([self.contingency_tbl.iloc[0].values])/np.array([self.contingency_tbl.iloc[2].values]) 
        self.HRG_ratio = self.HRG_ratio.tolist()[0]                         # Turning all the ratios into one list
        self.HRG_list = list(self.contingency_tbl.columns)                  # Getting all the headers in a list
        
        fig = go.Figure([go.Bar(x = self.HRG_list,                          # Creating plot based on the table
                                y = self.HRG_ratio)])    
        fig.update_xaxes(title_text="HRG")                                  # Setting x title
        fig.update_yaxes(title_text="Breach ratio (%)")                     # Setting y title
        fig.show()                                                          # Plotting
        
    def max_HRG_ratio(self):
        """
        max_HRG_ratio() is a method that print the list of the HRG that has biggest
        number of breaches.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """    
        HRG_high_ratio = []                                               # To identify which HRG has higher breach ratio than average
        for i in range(len(self.HRG_ratio)):                              # For loop through the whole table
            if self.HRG_ratio[i] > self.HRG_ratio[-1]:                    # If the current ratio is bigger than the overall ratio
                HRG_high_ratio.append(self.HRG_list[i])                   # Append the list to
        print("This HRG with ratio of breach above average are:", ", ".join(HRG_high_ratio))                                             # Printing all the HRG that have higher breach ratio that the average
        
    def analyse_HRG_amount(self):
        """
        analyse_HRG_amount() is a method that print the list of the HRG that has biggest
        number of breaches.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """    
        # Find out sensitivity to possible investment - same process as above
        self.HRG_breach_nbr = self.contingency_tbl[0:1].values.tolist()[0]

        fig = go.Figure([go.Bar(x = self.HRG_list, y = self.HRG_breach_nbr)])   # Creating bar plot of HRG with lot of breach and number of breach
        fig.update_xaxes(title_text="HRG")                                      # Set x title
        fig.update_yaxes(title_text="Number of breach")                         # Set y title
        fig.show()                                                              # Plotting
        
    def computing_improvement(self):
        """
        computing_improvement() is a method that calcul the % of improvement in breaches
        if we assume that it's equally hard to improve and plot a figure of the improved
        situation.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """    
        tot_breach = self.HRG_breach_nbr[9]                                  # Calculating total of breach
        after_reduction = []
        for i in self.HRG_breach_nbr[0:9]:                                   # Going through the list of breach number
            after_reduction.append(i * 0.1 / tot_breach * 100)               # Computing the effect of 10% of improvement
        
        fig = go.Figure([go.Bar(x = self.HRG_list, y = after_reduction)])    # Plotting the result in a bar chart
        fig.update_xaxes(title_text="HRG")                                   # Set x title
        fig.update_yaxes(title_text="overall improvement (%)")               # Set y title
        fig.show()                                                           # Plotting

    def further_analysis_VB08Z(self):
        """
        further_analysis_VB08Z() is a method that calcul the % of improvement in breaches
        if we assume that it's equally hard to improve and plot a figure of the improved
        situation.

        parameters: This methods doesn't require any arguments
    
        return: This methods doesn't return anything
        """ 
        # This is dataframe for 'VB08Z' and only include breach ones
        df_8z = self.df[(self.df["Breachornot"] == "breach" ) & (self.df["HRG"] == 'VB08Z')]
        print(df_8z)

        reduced_no = len(df_8z) - len(df_8z[df_8z["LoS"] * 0.9 > 240]) # the result will be the number of reduced breaches.
        print("The number of reduced breaches: %s" % reduced_no)

        overal_impv = reduced_no / len(self.df[(self.df["Breachornot"] == "breach" )])
        print("Overall improvement: %s" % round(overal_impv, 2))

        fig = px.scatter(self.df, x="noofinvestigation", y="LoS", color="Breachornot", trendline="ols")
        fig.show()
        df_8z[["LoS","noofinvestigation"]].corr()

        print("Number of breach 'VB08Z' with 'noofinvestigation' higher than average: %s" % len(df_8z[df_8z['noofinvestigation'] > self.df['noofinvestigation'].mean()]))
        
DataAnalysis = AnalyseData()


# ### First analysis
# To begin, we chose to plot a histogram of breach and a table of simple statistical values for each column of the *Sample*.

# In[3]:


if runOK:
    DataAnalysis.stat_and_breach()          # See the comments in method in class (above)
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Daily average time spent in hospital
# The following graph shows the timeseries of average LoS overall a whole month

# In[4]:


if runOK:                                            # See the comments in method in class (above)
    DataAnalysis.average_LOS_monthly()
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Boxplot of *LoSh* by *DayOfWeek*
# We are now trying to see what is happening on average each day using boxplot of *LoSh* by *DayOfWeek*

# In[5]:


if runOK:                                          # See the comments in method in class (above)
    DataAnalysis.LOS_by_DayOfWeek()
else:
    print("Cannot run anything in this cell due to libraries issue")
    


# ### Displaying number of patients in the hospital in an averageday

# In[6]:


if runOK:                                         # See the comments in method in class (above)
    DataAnalysis.average_patients_per_day()
else:
    print("Cannot run anything in this cell due to libraries issue")
    


# ### Scatter plot of *LoSh* by *Age*

# In[7]:


if runOK:                                           # See the comments in method in class (above)
    DataAnalysis.LOS_by_hour_by_age()
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Distribution of *LoSh*

# In[8]:


if runOK:                                            # See the comments in method in class (above)
    DataAnalysis.hist_LoSh()
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Repartition of the *LoS* in our Sample

# In[9]:


if runOK:                                         # See the comments in method in class (above)
    DataAnalysis.hist_time_spent_by_patient()
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### More histograms
# In this block of code we will display histograms of *Age*, *Day*, *Period*, *LoS*, *noofpatients* and trying to fit a model of distribution.

# In[10]:


if runOK:                                  # See the comments in method in class (above)
    DataAnalysis.hist_and_fitting_curve()
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Repartition of patients in the sample by age categories

# In[11]:


if runOK:                                     # See the comments in method in class (above)
    DataAnalysis.hist_by_age()
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Histograms
# Then we decided to plot histograms of *Age*, *HRG*, *Period* and *nooftreatment*, to have an idea of the distributions of the values, and maybe find a fitting law (normal for example).

# In[12]:


if runOK:                                              # See the comments in method in class (above)
    DataAnalysis.frequency_plots_of_breach()      
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Narrowing the analysis to HRG
# 
# We noticed that the *Age*, the *Period* and the *nooftreatment* had some random distributions that don't really give any clues about the lenght of stay (*LoS*).
# 
# This is why we decided to narrow the analysis to the *HRG* in order to see if there was a specific group that was more likely to *breach*.
# 
# The first step is conveyed in the next cell : we are analysis the contribution in number of breach of each HRG.

# In[13]:


if runOK:
    DataAnalysis.analyse_breach_HRG()        # See the comments in method in class (above)
else:
    print("Cannot run anything in this cell due to libraries issue")    


# ### What is the worst HRG ?

# In[14]:


if runOK:
    DataAnalysis.max_HRG_ratio()          # See the comments in method in class (above)
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Histogram of HRG in breach cases

# In[15]:


if runOK:
    DataAnalysis.analyse_HRG_amount()    # See the comments in method in class (above)
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Benefit of improvement
# In this part we assume that improving the *LoS* of every HRG is equally hard, and we try to minimise the overall *LoS* if we could **reduce one HRG by 10%**.

# In[16]:


if runOK:
    DataAnalysis.computing_improvement()    # See the comments in method in class (above)
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Result of the improvement on VB08Z

# In[17]:


if runOK:
    DataAnalysis.further_analysis_VB08Z()     # See the comments in method in class (above)
else:
    print("Cannot run anything in this cell due to libraries issue")


# ### Class Sample
# The following block of code contain the definition of the Class *Sample* which creates a sample and budnles all necessary methods to run operations in the GUI right after.

# In[18]:


class Sample:
    """
    This Class is a Super Class that is creating Data Samples for the GUI, but it is
    also bundling bunch of useful methods to perform action on the Sample.
    """
    
    def __init__(self, n: int, seed: int):
        """
        __init__() is the constructor of Sample. It is using pandas to generate a Sample
        and is computing the percentage of non breach.

        parameters: n, int and seed, int.
    
        return: This methods doesn't return anything.
        """
        self.df = data.sample(n = n, random_state = seed)
        self.percentage_non_breach = self.df["Breachornot"][self.df["Breachornot"] == "non-breach"].count() / self.df["Breachornot"].count()*100
    
    def get_percentage(self):
        """
        get_percentage() is a getter to get the percentage of non breach.

        parameters: This methods doesn't require any arguments.
    
        return: self.percentage_non_breach, float.
        """
        return self.percentage_non_breach
            
    def get_dataframe(self):
        """
        get_datagrame() is a getter to get the Sample created as a pandas DataFrame

        parameters: This methods doesn't require any arguments.
    
        return: self.df, pd.DataFrame.
        """
        return self.df
    
    def filter_by_condition(self, column, condition, value):
        """
        filter_by_condition() is methods that allows you to chose, based on
        conditions, which data in a specific column you want to keep. This has been thought
        to be use directly in terminal, this is why there are recursive call when values
        are not accepted, however it'll never happen when we will use it in the GUI.

        parameters: - column, str.
                    - condition, str.
                    - value, str.
    
        return: This methods isn't returning anything.
        """
        valid_conditions = ("==", "!=", "<=", ">=")             # Tuple of valid operators
        
        if column not in tuple(self.df.columns):                # Rejecting if column exist in the DataFrame
            column = input("Select a valid column in" + tuple(self.df.columns).join()) # Asking for a new value of column
            self.filter_by_condition(column, condition, value)  # Recursive call
            
        if condition not in valid_conditions:                   # Rejecting if the conditions isn't valid
            condition = input("Select a valid conditions between ==, !=, <= and >= : ") # Asking for a new value of value
            self.filter_by_condition(column, condition, value)  # Recursive call
            
        try:                                                    # This part of the try is to handle the case when
            value = float(value)                                # the chosen value can be casted as float (i.e. it's a number)
            evaluation = "self.df[\"" + column + "\"]" + condition + str(value)
            self.df = self.df[eval(evaluation)]                 # This line is actually filtering according to the parameters
            
            if self.df[column].dtype == "object":               # Rejecting if values is a number but the Column contains object
                column = input("Chose another column because this one doesn't support numerical condition : ")  # Recursive call
                self.filter_by_condition(column, condition, value)
                
        except:                                                 # And now we are dealing with the case of value being a str
            if self.df[column].dtype != "object":               # Rejecting a column that doesn't contain object
                column = input("Chose another column because this one doesn't support string condition : ")
                self.filter_by_condition(column, condition, value)    # Recursive call
                
            if condition in ("<=", ">="):                       # Rejecting condition operation "<=" and ">=" for str
                condition = input("You can only chose a condition between "==" or "!=" for this value : ")
                self.filter_by_condition(column, condition, value)    # Recursive call
            else:                                               # If nothing is rejecting we call finally proceed
                evaluation = "self.df[\"" + column + "\"]" + condition + "\"" + str(value) + "\""  # Actually filtering
                self.df = self.df[eval(evaluation)]             # Asking for a new value of column
    
    def add(self, column, condition, value):
        """
        add() is methods that allows you to chose, based on conditions, which data from the
        initial Data you want to add. This has not been made to be used in the terminal, so
        error exception will be made directly in the GUI.

        parameters: - column, str.
                    - condition, str.
                    - value, str.
    
        return: This methods isn't returning anything.
        """
        try:                              # Either the value is a number
            value = float(value)          # This is the line that will raise error if it's not a number
            evaluation = "data[data[\"" + column + "\"]" + condition + str(value) + "]"
            self.df = pd.concat([eval(evaluation),self.df], ignore_index = True).drop_duplicates().reset_index(drop=True)
        except:                           # Or it is a string  
            evaluation = "data[data[\"" + column + "\"]" + condition + "\"" + str(value) + "\"]"
            self.df = pd.concat([eval(evaluation),self.df], ignore_index = True).drop_duplicates().reset_index(drop=True)

    def remove_by_condition(self, column, condition, value):
        """
        remove_by_condition() is methods that allows you to chose, based on
        conditions, which data in a specific colmun you want to remove. This has been thought
        to be use directly in terminal, this is why there are recursive call when values
        are not accepted, however it'll never happen when we will use it in the GUI.
        Note that it's the reverse logic of filter_by_condition(), but everything else is the same.

        parameters: - column, str.
                    - condition, str.
                    - value, str.
    
        return: This methods isn't returning anything.
        """
        
        # For information about this code see the comments for filter_by_condition()
        valid_conditions = ("==", "!=", "<=", ">=")
        
        if column not in tuple(self.df.columns):
            column = input("Select a valid column in" + tuple(self.df.columns).join())
            self.filter_by_condition(column, condition, value)
            
        if condition not in valid_conditions:
            condition = input("Select a valid conditions between ==, !=, <= and >= : ")
            self.filter_by_condition(column, condition, value)
            
        try:
            
            value = float(value)
            evaluation = "self.df[\"" + column + "\"]" + condition + str(value)
            self.df = self.df.drop(self.df[eval(evaluation)].index)
            
            if self.df[column].dtype == "object":
                column = input("Chose another column because this one doesn't support numerical condition : ")
                self.remove_by_condition(column, condition, value)
                
        except:
            
            if self.df[column].dtype != "object":
                column = input("Chose another column because this one doesn't support string condition : ")
                self.remove_by_condition(column, condition, value)
                
            if condition in ("<=", ">="):
                condition = input("You can only chose a condition between "==" or "!=" for this value : ")
                self.remove_by_condition(column, condition, value)
                
            else:
                evaluation = "self.df[\"" + column + "\"]" + condition + "\"" + str(value) + "\""
                self.df = self.df.drop(self.df[eval(evaluation)].index)


# ### GUI
# This bloc of code contains both the Class *MakeWindow* in which every methods will be a window of the GUI and the *While True* loop that actually run the GUI

# In[19]:


### Library import ###

data = pd.read_csv("AED4weeks.csv")

############################## PyGui ###################################
class MakeWindow():
    """
    MakeWindow is a class that is bundling all the methods needed to create the
    windows of the GUI using the PySimpleGUI library.
    """
    def make_window1(self):
        """
        make_window1() is a method that creates the first window of the GUI.
        This window allows the user to generate a random sample of certain size,
        choosing a certain random seeds. All the data will be typed by the user.

        There is no parameters
    
        return: A PySimpleGUI Window
        """
        header_list = data.columns.values.tolist()        # This is a list of all column of the Sample
        
        # The layout is an argument of every PySimpleGUI window that contains the design of the window
        layout = [[sg.Text("Create sample", justification = "center", size = (1500,2), font = 25,)],
                  [sg.Text("Sample size ?"), sg.Input(k="-IN-", enable_events = True)],
                  [sg.Text("Random seed ?"), sg.Input(k="-IN2-", enable_events = True)],
                  [sg.Text(k="-OUTPUT-", justification = "center")],
                  [sg.Table(values = [header_list], auto_size_columns=False, k="-OUTPUT2-", \
                            size=(100,10), enable_events = True, visible = False, hide_vertical_scroll=True, justification = "center")],
                  [sg.Button("Compute")],
                  [sg.Button("Next >"), sg.Button("Exit")]]
        
        # Returning the window with title, it's resizable because finalize = False, but the default size is 1100x500
        return sg.Window("Crate Sample", layout, finalize=True, size=(1100,500) )
    
    def make_window2(self):
        """
        make_window2() is a method that creates the second window of the GUI.
        This window allows the user to select which Sample he wants to work on
        in a ListBox updated from the results of the first window?

        There is no parameters
    
        return: A PySimpleGUI Window
        """
        # The layout is an argument of every PySimpleGUI window that contains the design of the window
        layout = [[sg.Text("Select the sample you want to use", justification = "center", size = (1500,2), font = 25)],
                  [sg.Listbox(sample_list, size=(1200,5), key="column", enable_events = True)],
                  [sg.Text(size=(80,10),  k="-OUTPUT-")],
                  [sg.Button("< Prev"), sg.Button("Next >")]]
        
        # Returning the window with title, it's resizable because finalize = False, but the default size is 1100x500
        return sg.Window("Select the sample you want to use", layout, finalize=True, size=(1100,500) )
    
    def make_window3(self, DataFrame):
        """
        make_window3() is a method that creates the third window of the GUI.
        This window allows the user to modify the sample he generated by adding
        or removing row with conditions in Listbox.

        DataFrame: pd.DataFrame, it is the DataFrame to display in the middle Table
    
        return: A PySimpleGUI Window
        """
        # The layout is an argument of every PySimpleGUI window that contains the design of the window
        layout = [[sg.Text("Modify value(s) of the Data Sample")],
                  [sg.Listbox(list(data.columns), size=(20,4), key="column", enable_events = True),sg.T("      ") , sg.Listbox(["==", "!=", "<=", ">="], key = "condition", enable_events= True, size=(10,4)), sg.T("    Type your value after: "), sg.Input(k="-IN-", enable_events= True, size=(10,4)) ],
                  [sg.Text(size=(900,4),  k="-OUTPUT-", justification = "center")],
                  [sg.Table(values = DataFrame.values.tolist(), headings = DataFrame.columns.tolist(), size=(100,10), auto_size_columns = False, k="-OUTPUT2-", enable_events = True, visible = True, hide_vertical_scroll=True, justification = "center")],
                  [sg.Button("Filter"),sg.Button("Add"),sg.Button("Remove")],
                  [sg.Button("< Prev"), sg.Button("Next >")]]
        
        # Returning the window with title, it's resizable because finalize = False, but the default size is 1100x500
        return sg.Window("Modify value(s) of the Data Sample", layout, finalize=True, size=(1100,500) )

    def make_window4(self, DataFrame):
        """
        make_window4() is a method that creates the fourth window of the GUI.
        This window displays a summary of the sample data and useful plots.

        DataFrame: pd.DataFrame, it is the DataFrame to display in the middle Table
    
        return: A PySimpleGUI Window
        """
        summary = np.round(DataFrame.describe(),2)
        summary.insert(0," ",["count","mean","std", "min", "25%", "50%", "75%","max"])
        percentage =  DataFrame["Breachornot"][DataFrame["Breachornot"] == "non-breach"].count() / DataFrame["Breachornot"].count()*100
        
        # The layout is an argument of every PySimpleGUI window that contains the design of the window
        layout = [[sg.Text("Sample Summary", justification = "center", size = (1500,2), font = 25)], [sg.Text(f"Percentage of non-breach people {np.round(percentage,1)}%")],
                  [sg.Table(values = summary.values.tolist()[1:], headings=summary.columns.values.tolist(), auto_size_columns=False, hide_vertical_scroll=True, justification = "center", size=(1500,7))],
                  [sg.Text(" ", size=(1500,6))],
                  [sg.Button("< Prev"), sg.Button("Exit")]]
        
        # Returning the window with title, it's resizable because finalize = False, but the default size is 1100x50
        return sg.Window("Sample Summary", layout, finalize=True, size=(1100,500) )

if runOK:
    #Initialization of the GUI 
    sg.theme("DarkAmber")                         # Setting the theme
    windowMaker = MakeWindow()                    # Instantiating the class that is bundling every method to create windows
    window1, window2, window3, window4 = windowMaker.make_window1(), None, None, None  # Creating the first window
    sample_list = []                              # List that will contain every sample name
    sample_dict = {}                              # dict that will pair every sample name with its value
    selected_sample = "Empty"                     # Value that contains the selected sample, "Empty" at the begining

    while True:
        window, event, values = sg.read_all_windows()        # Window is the current window,
                                                             # Event is a list of str representing events in the window
                                                             # value is a dict of events and values corresponding

        if window == window1 and event in (sg.WIN_CLOSED, "Exit"):  # Quit if press exit or close in the first window
            break

        if window == window1:                                # Behaviour for first window
            if event == "Next >":                            # If next button is selected
                window1.hide()                               # Hide the first window
                window2 = windowMaker.make_window2()         # And create and chose the second window as active
            elif event == "Compute":                         # If compute button is selected
                if values["-IN-"] == "" and values["-IN2-"] == "":  # Rejecting the case of empty value as input
                    window1["-OUTPUT-"].update("Please do not let any of the fields empty")
                else:                                               # If values are not empty
                    try:                                            # Rejecting every values that cannot be casted as int
                        sample = Sample(int(values["-IN-"]),int(values["-IN2-"]))   # Creating the Sample
                        SampleName = "Sample : n = " + values["-IN-"] + ", seed = " + values["-IN2-"]  # Generating a unique name for this Sample
                        df2 = sample.get_dataframe()
                        data2 = df2.values.tolist()
                        window1["-OUTPUT2-"].update(data2, visible=True)            # Showing the Sample in the Table of the window

                        if SampleName not in sample_list:            # Accepting the case of new sample
                            sample_list.append(SampleName)
                            sample_dict[SampleName] = sample
                        else:                                        # Rejecting because the sample already exists
                            window1["-OUTPUT-"].update("This Data Sample already exists")
                    except:                                          # Rejected if values are not numbers
                        window1["-OUTPUT-"].update("Size and seed must be numbers")

        if window == window2:                                      # If we are working in window2
            if event == "Next >":                                  # If the Next button is used
                if selected_sample != "Empty":                     # We can go on the next window only if we selected something
                    window2.hide()                                 # Hiding window2
                    df3 = selected_sample.get_dataframe()
                    window3 = windowMaker.make_window3(df3)        # Creating and switching to window3
            elif event in (sg.WIN_CLOSED, "< Prev"):               # If we hit close or Prev
                window2.close()                                    # We close the window2
                window1.un_hide()                                  # And get back to window1
            else:
                selected_sample = sample_dict[values[event][0]]    # If we are clicking on the ListBox, we are selected the Sample

        if window == window3:                                      # If we are working in window 3
            if event == "Filter":                                  # If we want to use the Filter button
                try:                                               # Trying to chose each of 3 values, ie none is empty
                    column = str(values["column"][0])
                    condition = str(values["condition"][0])
                    value = str(values["-IN-"])

                    try:                                           # Filtering data if the value can be casted as float
                        float(value)                               # This is the line that may raise the error
                        if data[column].dtype != "int64":          # Rejecting the case where value is a number but the column isn't containing number
                            window3["-OUTPUT-"].update("You cannot use a numerical condition for this column")
                        else:                                      # Else filtering, displaying and informing
                            selected_sample.filter_by_condition(column, condition, value)
                            window3["-OUTPUT-"].update("Sucessfully filtered the DataFrame")
                            window3["-OUTPUT2-"].update(values = selected_sample.get_dataframe().values.tolist())
                            df3 = selected_sample.get_dataframe()
                    except:                                         # If the value cannot be casted as float, ie it's a string
                        if value not in list(selected_sample.get_dataframe()[column]):             # Warning the user if he tries to filter by a value that doesn' exist
                            window3["-OUTPUT-"].update("This value isn't in this column") 
                        if data[column].dtype == "int64":                                          # Rejecting column that contains int because value is a float
                            window3["-OUTPUT-"].update("Cannot use a str value for this column")
                        elif condition not in ("!=", "=="):                                        # Rejecting "<=" and ">=" operator because we are working with strings
                            window3["-OUTPUT-"].update("You can only use == and != as conditions for this type of column")
                        else:                                                                      # Actually filtering if nothing is rejected
                            selected_sample.filter_by_condition(column, condition, value)
                            window3["-OUTPUT-"].update("Sucessfully filtered the DataFrame")
                            window3["-OUTPUT2-"].update(values = selected_sample.get_dataframe().values.tolist())
                            df3 = selected_sample.get_dataframe()
                except:                                                                            # Warning the user that there's at least one value that is empty
                    window3["-OUTPUT-"].update("You need to chose values before removing")

            if event == "Remove":        # If the button selected is remove
                # The logic of if/else and try/except in this condition is the same as above. 
                try:
                    column = str(values["column"][0])
                    condition = str(values["condition"][0])
                    value = str(values["-IN-"])

                    try:
                        float(value)
                        if data[column].dtype != "int64":
                            window3["-OUTPUT-"].update("You cannot use a numerical condition for this column")
                        else:   
                            selected_sample.remove_by_condition(column, condition, value)
                            window3["-OUTPUT-"].update("Sucessfully filtered the DataFrame")
                            window3["-OUTPUT2-"].update(values = selected_sample.get_dataframe().values.tolist())
                            df3=selected_sample.get_dataframe()
                    except:
                        if value not in list(selected_sample.get_dataframe()[column]):
                            window3["-OUTPUT-"].update("Don't use this value, it isn't in this column") 
                        if data[column].dtype == "int64":
                            window3["-OUTPUT-"].update("Cannot use an str value for this column")
                        elif condition not in ("!=", "=="):
                            window3["-OUTPUT-"].update("You can only use == and != as conditions for this type of column")
                        else:
                            selected_sample.remove_by_condition(column, condition, value)
                            window3["-OUTPUT-"].update("Sucessfully filtered the DataFrame")
                            window3["-OUTPUT2-"].update(values = selected_sample.get_dataframe().values.tolist())
                            df3=selected_sample.get_dataframe()
                except:
                    window3["-OUTPUT-"].update("You need to chose values before filtering")
            elif event == "Add":          # If the selected button is Add
                # Once again the logic of if/else and try/except is the same as above.
                try:
                    column = str(values["column"][0])
                    condition = str(values["condition"][0])
                    value = str(values["-IN-"])

                    try:
                        float(value)
                        if data[column].dtype != "int64":
                            window3["-OUTPUT-"].update("You cannot use a numerical condition for this column")
                        else:   
                            selected_sample.add(column, condition, value)
                            window3["-OUTPUT-"].update("Sucessfully filtered the DataFrame")
                            window3["-OUTPUT2-"].update(values = selected_sample.get_dataframe().values.tolist())
                            df3=selected_sample.get_dataframe()
                    except:
                        if value not in list(selected_sample.get_dataframe()[column]):
                            window3["-OUTPUT-"].update("Don't use this value, it isn't in this column") 
                        if data[column].dtype == "int64":
                            window3["-OUTPUT-"].update("Cannot use an str value for this column")
                        elif condition not in ("!=", "=="):
                            window3["-OUTPUT-"].update("You can only use == and != as conditions for this type of column")
                        else:
                            selected_sample.add(column, condition, value)
                            window3["-OUTPUT-"].update("Sucessfully filtered the DataFrame")
                            window3["-OUTPUT2-"].update(values = selected_sample.get_dataframe().values.tolist())
                            df3=selected_sample.get_dataframe()
                except:
                    window3["-OUTPUT-"].update("You need to chose values before adding")

            elif event in (sg.WIN_CLOSED, "< Prev"):          # If we press Prev or Close
                window3.close()                               # We hide the window3
                window2.un_hide()                             # And go back to window2
            elif event == "Next >":                           # If we press Next
                window3.hide()                                # We hide window3
                window4 = windowMaker.make_window4(df3)       # And we create the window4 (the last one)

        if window == window4 and event in (sg.WIN_CLOSED, 'Exit'): # If Exit or Close is selected we close everything
            break
        if window == window4:                                 # When the window4 is active
            if event in (sg.WIN_CLOSED, "< Prev"):            # Go backward if Prev is pressed
                window4.close()
                window3.un_hide()

    window.close()                                            # Once we exited the While true because we pressed exit, we actually close the window

    if selected_sample == "Empty":                            # Handling the case where the GUI is exited too early
        pass
    else:
        print(selected_sample.get_dataframe())                       # Otherwise show the sample we computed with the GUI
else:
    print("Cannot run anything in this cell due to libraries issue")

